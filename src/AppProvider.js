import React from 'react';
const AppContext = React.createContext({ plates: [], username: "" });


class AppProvider extends React.Component {
    static Consumer = AppContext.Consumer;
    state = {
        user: {},
        token: ""
    }

    onChange = (user, token) => {
        console.log('123', user.id);
        this.setState({
            user, token
        });
    }
    render() {
        return (
            <AppContext.Provider value={{ user: this.state.user, token: this.state.token, onChange: this.onChange }}>
                {this.props.children}
            </ AppContext.Provider>
        )
    }

}
export default AppProvider;