import React, { Component } from 'react';
import './App.css';
import LandingPage from './screens/LandingPage';
import AppProvider from './AppProvider';
class App extends Component {
  render() {
    return (
      <LandingPage />
    )
  }
}
const WrapperContext = (props) => {
  return (<AppProvider>
    <App {...props}></App>
  </AppProvider>)
}
export default WrapperContext;
