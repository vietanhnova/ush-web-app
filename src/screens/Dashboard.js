import React from 'react'
import { Grid } from '@material-ui/core';
import AreaTabs from '../components/AreaTabs';
import Clock from '../components/Clock'
import Weather from '../components/Weather'
import Account from '../components/Account'
import SharedDevices from '../components/SharedDevices'
// import IShareDevices from '../components/IShareDevices'

export default class DashBoard extends React.Component {
    render() {
        return (
            <div className="dashboard">
                <Grid container>
                    <Grid item xs={3}>
                        <Grid container direction="row"
                            justify="center"
                            alignItems="center">
                            <Grid item xs={12}>
                                <Clock />
                                <Weather />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <Grid container direction="row"
                            justify="center"
                            alignItems="center">
                            <Grid item xs={12}>
                                <AreaTabs />
                                <h2>Shared devices</h2>
                                <SharedDevices />
                                <h2>Shared User</h2>
                                {/* <IShareDevices /> */}
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={3}>
                        <Grid container direction="row"
                            justify="center"
                            alignItems="center">
                            <Account />
                        </Grid>
                    </Grid>

                </Grid>
            </div>
        )
    }
}