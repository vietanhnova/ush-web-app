import React from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import AppProvider from '../AppProvider'
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    }
  }
  changeEmail = (event) => {
    let value = event.target.value;
    this.setState({ email: value })
  };
  changePass = (event) => {
    let value = event.target.value;
    this.setState({ password: value })
  };
  handleLogin = () => {
    axios.post('http://ush-server.ddns.net/api/user/reg/pw', {
      email: this.state.email,
      password: this.state.password,
      isLogin: true
    })
      .then(response => {
        console.log(response.data.jwtToken);
        this.props.onChange(response.data.data, response.data.jwtToken);
        this.props.login();
      })
      .catch(function (error) {
        console.log(error);
      });


  }
  render() {
    console.log(this.props)
    return (
      <Grid container
        direction="row"
        justify="center"
        alignItems="center">
        <Grid item xs={4} container
          direction="row"
          justify="center"
          alignItems="center">
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={this.changeEmail.bind(this)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={this.changePass.bind(this)}
          />
          <Button variant="contained" onClick={this.handleLogin.bind(this)}>Login</Button>
        </Grid>
      </Grid>
    )
  }
}
const WrapperContext = (props) => {
  return (<AppProvider.Consumer>
    {context => { return <Login {...props} {...context}></Login> }}
  </AppProvider.Consumer>)
}
export default WrapperContext;