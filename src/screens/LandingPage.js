import React from 'react';
import { Grid, Button } from '@material-ui/core';
import homeControl from '../images/bannerImg.svg';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
    useHistory,
    useLocation
} from "react-router-dom";
import Login from './Login'
import Dashboard from './Dashboard'
import Home from './Home'
import AppProvider from '../AppProvider'
class LandingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isHidden: false
        }
    }
    handleHidden() {
        this.setState({ isHidden: true })
    }
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/login">
                        <LoginPage />
                    </Route>
                    <PrivateRoute path="/dashboard">
                        <Dashboard />
                    </PrivateRoute>
                </Switch>
                {
                    this.state.isHidden ? "" : (<div className="landing-page" >
                        <Grid container
                            direction="row"
                            justify="center"
                            alignItems="center">
                            <Grid item xs={4} container
                                direction="row"
                                justify="center"
                                alignItems="center">
                                <h1 style={{ color: "white", fontSize: 40 }}>UTE SMART HOME</h1>
                                <h2 style={{ color: "white" }}>Control home from anywhere</h2>
                                <Link to="/dashboard" onClick={this.handleHidden.bind(this)}>
                                    <Button style={{ color: "white", backgroundColor: "#1B0F34" }}
                                        variant="contained">
                                        Get Started
                                </Button>
                                </Link>
                                <Link to="/home" onClick={this.handleHidden.bind(this)}>
                                    <Button style={{ color: "white", backgroundColor: "#1B0F34" }}
                                        variant="contained">
                                        Learn More
                                </Button>
                                </Link>

                            </Grid>
                            <Grid item xs={8}>
                                <div>
                                    <img src={homeControl} alt="iot" />
                                </div>
                            </Grid>
                        </Grid>
                    </div>)
                }
            </Router>
        );
    }
}
const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
        fakeAuth.isAuthenticated = true;
        setTimeout(cb, 100); // fake async
    },
    signout(cb) {
        fakeAuth.isAuthenticated = false;
        setTimeout(cb, 100);
    }
};


function PrivateRoute({ children, ...rest }) {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                fakeAuth.isAuthenticated ? (
                    children
                ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: location }
                            }}
                        />
                    )
            }
        />
    );
}

function LoginPage() {
    let history = useHistory();
    let location = useLocation();

    let { from } = location.state || { from: { pathname: "/" } };
    let login = () => {
        fakeAuth.authenticate(() => {
            history.replace(from);
        });
    };

    return (
        <Login login={login.bind(this)} />
    );
}
const WrapperContext = (props) => {
    return (<AppProvider>
        <LandingPage {...props}></LandingPage>
    </AppProvider>)
}
export default WrapperContext;