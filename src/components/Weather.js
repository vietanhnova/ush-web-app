import React from 'react'
const x = `
  <html>
    <script id="myScript">
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
    </script>
    <body>
    <a class="weatherwidget-io" href="https://forecast7.com/en/10d82106d63/ho-chi-minh-city/" data-label_1="HO CHI MINH CITY" data-label_2="WEATHER" data-icons="Climacons Animated" data-theme="pure" data-basecolor="rgba(255, 255, 255, 0)" data-textcolor="#ffffff" data-lowcolor="#ffffff" >HO CHI MINH CITY WEATHER</a>
    </body>
  </html>
`;

export default class Weather extends React.Component {

    componentDidMount() {
        const script = document.getElementById('myScript').innerHTML;
        window.eval(script);
    }

    render() {
        return <div dangerouslySetInnerHTML={{ __html: x }} />;
    }

};