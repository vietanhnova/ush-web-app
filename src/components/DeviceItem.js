import React from 'react'
import {
    Button, Grid, Switch,
    DialogTitle, Dialog,
    DialogContent, DialogActions,
    Slider, Input
} from '@material-ui/core';
import EmojiObjectsOutlinedIcon from '@material-ui/icons/EmojiObjectsOutlined';
import ToysOutlinedIcon from '@material-ui/icons/ToysOutlined';
import ShareIcon from '@material-ui/icons/Share';
import AppProvider from '../AppProvider'
class DeviceItem extends React.Component {
    state = {
        isOpen: false
    }
    handleDialog = () => {
        if (this.state.isOpen) {
            this.setState({ isOpen: false });
        } else {
            this.setState({ isOpen: true });
        }
    }
    render() {
        const device = this.props.device;
        const buttonStyle = device.powerOn ?
            { fontSize: 40, color: "yellow" }
            : { fontSize: 40, color: "black" };
        var deviceIcon;
        if (device.signalTypeId == 1) {
            deviceIcon = <Button><EmojiObjectsOutlinedIcon style={buttonStyle} /></Button>
        }
        if (device.signalTypeId == 2) {
            deviceIcon = <Button><ToysOutlinedIcon style={buttonStyle} /></Button>
        }
        var isSharedDevice = false;
        if (this.props.user.id != device.createdUser) {
            isSharedDevice = true;
        }
        return (
            <div>
                <Grid container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    style={{ backgroundColor: 'rgba(238,238,238,0.6)', color: "black" }} >
                    <Grid item container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item xs={6}>
                            {deviceIcon}
                        </Grid>
                        <Grid item xs={6}>
                            <Switch size="small" />
                            <div>{device.name}</div>
                        </Grid>
                    </Grid>
                    {isSharedDevice && (<Grid item>
                        <Button><ShareIcon style={{ fontSize: 20 }} />{device.createdUser}</Button>
                    </Grid>)}
                </Grid>
                <div>
                    <Dialog open={this.state.isOpen}>
                        <DialogTitle >
                            {device.name}
                        </DialogTitle>
                        <DialogContent>
                            <Grid container spacing={1} alignItems="center">
                                <Grid item>
                                    <ToysOutlinedIcon />
                                </Grid>
                                <Grid item xs>
                                    <Slider
                                        value={4}
                                        aria-labelledby="input-slider"
                                    />
                                </Grid>
                                <Grid item>
                                    <Input
                                        value={4}
                                        margin="dense"
                                        inputProps={{
                                            step: 1,
                                            min: 0,
                                            max: 10,
                                            type: 'number',
                                            'aria-labelledby': 'input-slider',
                                        }}
                                    />
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleDialog.bind(this)} color="primary">
                                Save
                            </Button>
                            <Button onClick={this.handleDialog.bind(this)} color="primary" autoFocus>
                                Cancel
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
            </div>
        )
    }
}
const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <DeviceItem {...props} {...context}></DeviceItem> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;