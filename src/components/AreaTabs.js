import React from 'react';
import PropTypes from 'prop-types';
import {
    Tabs, Tab, Typography, Box, Button, Grid, DialogTitle, Dialog,
    DialogContent, DialogActions,
    TextField
} from '@material-ui/core';
import AreaItem from './AreaItem';
import AppProvider from '../AppProvider'
import EditIcon from '@material-ui/icons/Edit';
import AddArea from './AddArea';
function TabPanel(props) {
    const { children, value, item, index, ...other } = props;
    console.log(props);
    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={2}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

class AreaTabs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            tabId: "",
            value: 0,
            areas: []
        }
    }
    getData() {
        fetch("http://ush-server.ddns.net/api/area", {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.token
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    areas: responseJson,
                });
                console.log(responseJson);
            })
            .catch(error => console.log(error))
    }
    componentDidMount() {
        this.getData();
    }
    handleChange = (event, newValue) => {
        let id = this.state.areas[newValue].id;
        this.setState({ value: newValue, tabId: id });
    };
    handleDialog = () => {
        if (this.state.isOpen) {
            this.setState({ isOpen: false });
        } else {
            this.setState({ isOpen: true });
        }
    }
    changeAreaName = (event) => {
        let name = event.target.value;
        this.setState({ areaName: name })
    }
    editArea = () => {
        console.log("edit", this.state);
        fetch("http://ush-server.ddns.net/api/area", {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.token
            },
            body: {
                id: this.state.tabId,
                name: this.state.areaName
            }
        }).then(response => {
            this.handleDialog();
            this.getData();
            console.log(response);
        })
            .catch(error => console.log(error))
    }
    render() {
        return (
            <div>
                <Grid container direction="row">
                    <Grid item xs={10}>
                        <Tabs value={this.state.value}
                            onChange={this.handleChange.bind(this)}
                            indicatorColor="primary"
                            textColor="primary"
                            variant="scrollable"
                            scrollButtons="auto"
                            aria-label="scrollable auto tabs example">
                            {
                                this.state.areas.map((item, i) => (
                                    <Tab label={
                                        <Grid>{item.name}
                                            <Button variant="text" onClick={this.handleDialog.bind(this)}>
                                                <EditIcon style={{ fontSie: 12 }} />
                                            </Button>
                                        </Grid>}
                                        key={i} {...a11yProps(i)} />
                                ))
                            }
                        </Tabs>
                    </Grid>
                    <Grid item xs={2}>
                        <AddArea token={this.props.token} getData={this.getData.bind(this)} />
                    </Grid>
                </Grid>
                {
                    this.state.areas.map((item, i) => (
                        <TabPanel value={this.state.value} index={i} key={i}>
                            <div>
                                <AreaItem area={item} />
                            </div>
                            <Dialog open={this.state.isOpen}>
                                <DialogTitle >
                                    Edit area
                            </DialogTitle>
                                <DialogContent>
                                    <TextField
                                        defaultValue={item.name}
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="email"
                                        label="Area name"
                                        name="area"
                                        autoComplete="email"
                                        autoFocus
                                        onChange={this.changeAreaName.bind(this)}
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.editArea.bind(this)} color="primary">
                                        Update
                            </Button>
                                    <Button onClick={this.handleDialog.bind(this)} color="primary" autoFocus>
                                        Cancel
                            </Button>
                                </DialogActions>
                            </Dialog>
                        </TabPanel>
                    ))
                }

            </div>
        );
    }
}
const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <AreaTabs {...props} {...context}></AreaTabs> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;