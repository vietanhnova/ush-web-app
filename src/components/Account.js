import React from 'react';
import {
    Button, Grid, Avatar,
    DialogTitle, Dialog,
    DialogContent, DialogActions,
    TextField
} from '@material-ui/core';
import AppProvider from '../AppProvider';
class Account extends React.Component {
    state = {
        isOpen: false
    }
    handleDialog = () => {
        if (this.state.isOpen) {
            this.setState({ isOpen: false });
        } else {
            this.setState({ isOpen: true });
        }
    }
    render() {
        const user = this.props.user;
        return (
            <Grid container
                direction="row"
                justify="flex-end"
                alignItems="flex-end">
                <Button onClick={this.handleDialog.bind(this)} >
                    <Avatar variant="circle" alt="avt" src={user.image} />
                    {user.fullName}
                </Button>
                <Dialog open={this.state.isOpen}>
                    <DialogTitle >
                        Account Info
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            // onChange={this.changeEmail.bind(this)}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            // onChange={this.changePass.bind(this)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialog.bind(this)} color="primary">
                            Save
                            </Button>
                        <Button onClick={this.handleDialog.bind(this)} color="primary" autoFocus>
                            Cancel
                            </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        )
    }
}

const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <Account {...props} {...context}></Account> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;