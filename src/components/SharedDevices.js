import React from 'react'
import { Grid, Box } from '@material-ui/core';
import DeviceItem from './DeviceItem';
export default class SharedDevices extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            devives: []
        }
    }
    getData() {
        fetch('http://ush-server.ddns.net/api/device/listShareDevice', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InVzZXIxIiwiaWF0IjoxNTg3MTg0NzI1LCJleHAiOjE5MDI1NDQ3MjV9.1sSgeT8R5Ws-H9DTcXQXcK2QujawXtIyd_brIMOB6pQ'
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    devives: responseJson,
                });
                console.log(responseJson);
            })
            .catch(error => console.log(error))
    }
    componentDidMount() {
        this.getData();
    }
    render() {
        return (
            <Box p={2}>
                <Grid container spacing={3}>
                    {
                        this.state.devives.map((item, i) => (
                            <Grid item xs={3} key={i}>
                                <DeviceItem device={item} />
                            </Grid>
                        ))}
                </Grid>
            </Box>

        )
    }
}