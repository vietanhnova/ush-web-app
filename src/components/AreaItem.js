import React from 'react'
import { Grid } from '@material-ui/core';
import DeviceItem from './DeviceItem';
import AppProvider from '../AppProvider'

class AreaItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            devives: []
        }
    }
    getData() {
        let areaId = this.props.area.id;
        fetch(`http://ush-server.ddns.net/api/device/listByArea/${areaId}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.token
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    devives: responseJson,
                });
                console.log(responseJson);
            })
            .catch(error => console.log(error))
    }
    componentDidMount() {
        this.getData();
    }
    render() {
        return (
            <div>
                <Grid container spacing={3}>
                    {
                        this.state.devives.map((item, i) => (
                            <Grid item xs={3} key={i}>
                                <DeviceItem device={item} />
                            </Grid>
                        ))}
                </Grid>
            </div>

        )
    }
}
const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <AreaItem {...props} {...context}></AreaItem> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;