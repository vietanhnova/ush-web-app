import React from 'react'
import {
    Button,
    DialogTitle, Dialog,
    DialogContent, DialogActions,
    TextField
} from '@material-ui/core';
import AddBoxIcon from '@material-ui/icons/AddBox';
import axios from 'axios';
export default class AddArea extends React.Component {
    state = {
        isOpen: false,
        areaName: ""
    }
    handleDialog = () => {
        if (this.state.isOpen) {
            this.setState({ isOpen: false });
        } else {
            this.setState({ isOpen: true });
        }
    }
    changeAreaName = (event) => {
        let name = event.target.value;
        this.setState({ areaName: name })
    }
    addArea = () => {
        fetch("http://ush-server.ddns.net/api/area", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.token
            },
            body: {
                name: this.state.areaName
            }
        })
            .then(response => {
                this.handleDialog();
                this.props.getData();
                console.log(response);
            })
            .catch(error => console.log(error))
    }
    render() {
        return (
            <div>
                <Button onClick={this.handleDialog.bind(this)}><AddBoxIcon /></Button>
                <Dialog open={this.state.isOpen}>
                    <DialogTitle >
                        Add a new area
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Area name"
                            name="area"
                            autoComplete="email"
                            autoFocus
                            onChange={this.changeAreaName.bind(this)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialog.bind(this)} color="primary">
                            Add
                            </Button>
                        <Button onClick={this.handleDialog.bind(this)} color="primary" autoFocus>
                            Cancel
                            </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}