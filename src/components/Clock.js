import React from 'react';
import AnalogueClock from 'react-analogue-clock';
function Clock() {
    const clockOptions = {
        baseColor: 'rgba(255, 255, 255, 0)',
        borderColor: '#ffffff',
        borderWidth: 0,
        centerColor: '#ffffff',
        handColors: {
            hour: '#ffffff',
            minute: '#ffffff',
            second: '#ffffff',
        },
        notchColor: '#ffffff',
        numbersColor: '#ffffff',
        showNumbers: true,
        size: 300
    }
    return (
        <AnalogueClock {...clockOptions} />
    )
}
export default Clock;